#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include<time.h>

int** matrix;
int N;
int maxDelta;

void writeData()
	{
		char outwardFile[256];
//		sprintf(outwardFile,"outward_Data_%d",N);
		sprintf(outwardFile,"outward_Data");
		
		char printFile[256];
		sprintf(printFile,"graph.dot");
		
		char inwardFile[256];
//		sprintf(inwardFile,"inward_Data_%d",N);
		sprintf(inwardFile,"inward_Data");

//		char filename[]="inward_Data";
		//sprintf(filename,"%d_data",pid); // = "matrixData_8_1";
		FILE* infp=fopen(inwardFile, "w");
		FILE* outfp=fopen(outwardFile, "w");
		
		FILE* printfp=fopen(printFile, "w");

		fprintf(infp,"%d\n",N);
				
		fprintf(printfp,"digraph {\n");
		
		int i,j,k;
		int outDelta=0;
		
//		char line[N*4];
		
		for(i=0;i<N;i++){
			outDelta=0;
			
			for(j=0;j<N;j++){
				if(matrix[i][j]==1){
					outDelta++;
				}
			}
			fprintf(outfp,"%d",outDelta);
			for(j=0;j<N;j++){
				if(matrix[i][j]==1){
					fprintf(outfp,",%d",j);
					fprintf(printfp,"a%d -> a%d\n",i,j);
				}
			}
			fprintf(outfp,"\n");

		}
		
		int inDelta=0;
		for(i=0;i<N;i++){
			inDelta=0;
			
			for(j=0;j<N;j++){
				if(matrix[j][i]==1){
					inDelta++;
				}
			}
			fprintf(infp,"%d",inDelta);
			for(j=0;j<N;j++){
				if(matrix[j][i]==1){
					fprintf(infp,",%d",j);
					
				}
			}
			fprintf(infp,"\n");
		}
		fprintf(printfp,"}\n");
		
		fclose(printfp);
		fclose(infp);
		fclose(outfp);
	}



int main(int argc,char* argv[]){


	if(argc<3){
		printf("Usage ./a.out N maxDelta\n");
		exit(1);
	}
	
	N = atoi(argv[1]); 
	maxDelta = atoi(argv[2]);

	matrix=(int**)malloc(sizeof(int*)*N);
	
	int i;
	for(i=0;i<N;i++){
		matrix[i] = (int*)malloc(sizeof(int)*N);
	}
	
	srand(time(NULL));
	int j;
	int delta=0;
	for(i=0;i<N;i++){
		delta=0;
//		if(i%2==0){
			for(j=0;j<N;j++){
				
				
				if(i==j || delta>maxDelta || j-i>5 || i-j>5){
					matrix[i][j] =0;	
				}else{
					matrix[i][j]=rand()%2;
					if(matrix[i][j]==1){
						delta++;
					}
				}
			}
// 		}else{
// 		
// 			for(j=N-1;j>-1;j--){
// 				
// 				
// 				if(i==j || delta>maxDelta ){
// 					matrix[i][j] =0;	
// 				}else{
// 					matrix[i][j]=rand()%2;
// 					if(matrix[i][j]==1){
// 						delta++;
// 					}
// 				}
// 			}
// 		}
	}
	
	writeData();
	
	
}
