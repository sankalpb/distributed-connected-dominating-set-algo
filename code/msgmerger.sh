#!/bin/bash
echo $1
t3file="msgFilemerged"
if [ -f $t3file ]; then
rm $t3file
fi

for (( i = 0 ; i < $1; i++ )) ### Inner for loop ###
do
t2file="msgFile_${i}"

echo "Msg data of node ${i}\n" >> $t3file

cat $t2file >> $t3file
done

