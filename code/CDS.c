#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>

#define MaxNeighbours 100
#define MAXNODES 100
#define BUFFSIZE 10000000

#define LEADER_SEARCH 2

#define DEBUG 0

int pid;
int P;
int glbMsgSent=0;
int glbMsgRcvd=0;
char MsgFileName[30];
FILE* msgFP;
typedef struct{
	int id;
	int isBlack;
	int isRemoved;
}NodeId;

typedef struct{
	
	NodeId fwdEdgeNbrs[MaxNeighbours];
	NodeId bwdEdgeNbrs[MaxNeighbours];
	int inwardDelta;
	int outwardDelta;
	
	int orgInwardDelta;
	int orgOutwardDelta;
	
	int sizeOfD;
	
	int FNodes[MaxNeighbours];
	int sizeOfF;
	int isInSCC;
	
	int nodeSCC[MAXNODES];
	int sizeOfSCC;
	
	
	// Pruning Data
	int inGraph[MAXNODES][MAXNODES];
	int outGraph[MAXNODES][MAXNODES];
	
	int pruneIt;
	int inwardCDS[MAXNODES][MAXNODES];
	int outwardCDS[MAXNODES][MAXNODES];
	
	int inwardUnionSize;
	int outwardUnionSize;
	
}Node;
int fcount=0;		
/*		
			
Data Structure required at each node.

Info of All neighbours--Array of ids

*/
void printGraphLeader(Node *node, char **s);
void getColor1(int leader,char* color);
void getColor2(int leader,char* color);

//Choose leader function
int chooseSingleLeader(Node* node){

	//Send its Delta to all neighbours
//	printf("2####%x\n", node);
	int i;
	int mssgRcvd[MAXNODES];
	for(i=0;i<MAXNODES;i++){
		mssgRcvd[i]=0;
	}
//	mssgRcvd[pid]=1;
	
	int numMssgRcvd = 0;

	int delta = node->inwardDelta+node->outwardDelta;

	int leaderId = pid;
	int sent=0, recvd=0;
	int myMessageNum[MAXNODES];
	myMessageNum[0] = delta*100+pid;

	int sending=0;

	for(i=0;i<node->orgOutwardDelta;i++){
	
		MPI_Request request;

//		MPI_Isend(&myMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD,&request);
		MPI_Bsend(myMessageNum,100, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD);
		sent++;
		if(DEBUG) printf("Sending Message from %d to %d, srcID=%d\n",pid,node->fwdEdgeNbrs[i].id,pid);
	}

	
	numMssgRcvd=0;
// 	MPI_Request request1;

	while(numMssgRcvd < (node->sizeOfD)-1){
	
			MPI_Status status; 

		int senderId;
		int rcvdMeesageNum[100];

//		if(DEBUG) printf("PID=%d Waiting for message numMssgRcvd= %d\n",pid,numMssgRcvd);

		MPI_Recv(rcvdMeesageNum, 100,MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
		recvd++;
		senderId = status.MPI_SOURCE;
		
		int messageSrcId = rcvdMeesageNum[0]%100;
		int messageDelta = rcvdMeesageNum[0]/100;

		if(DEBUG) printf("Received Message from %d to %d, srcID=%d\n",senderId,pid,messageSrcId);

		if(messageSrcId==pid){
					printf("FATAL ERROR.............................:PID=%d gets its own message\n",pid);
		}

		if(mssgRcvd[messageSrcId]==0){
			
//			if(DEBUG) printf("Havent forwarded this message yet\n");

			if(delta>messageDelta){
				delta=messageDelta;
				leaderId = messageSrcId;
			}else if(delta==messageDelta){
				if(leaderId>messageSrcId){
					leaderId=messageSrcId;
				}
			}
			
			
//			if(DEBUG) printf("Going to forward the message \n");

			numMssgRcvd++;
			mssgRcvd[messageSrcId]=1;

			int forwarding=0;			

			for(i=0;i<node->orgOutwardDelta;i++){
				
// 				if(node->fwdEdgeNbrs[i].id!=senderId && node->fwdEdgeNbrs[i].id!=messageSrcId && node->fwdEdgeNbrs[i].id!=pid && senderId!=pid && messageSrcId!=pid){

				if(node->fwdEdgeNbrs[i].id!=senderId && node->fwdEdgeNbrs[i].id!=messageSrcId && messageSrcId!=pid){

					int fwdMessageNum[100];
					fwdMessageNum[0] = rcvdMeesageNum[0];
		
					MPI_Bsend(fwdMessageNum,100, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD);
					
/*					MPI_Isend(&fwdMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD,&request1);*/

					if(DEBUG) printf("Sent Message from %d to %d, srcID=%d\n",pid,node->fwdEdgeNbrs[i].id,fwdMessageNum);

				}else{
					if(DEBUG) printf("Message can not be sent to %d from %d...isRemoved=%d,senderId %d\n",node->fwdEdgeNbrs[i].id,pid,node->fwdEdgeNbrs[i].isRemoved,senderId);
				}
			}
		}
	}
	printf("PID=%d Leader choosed: leaderId=%d..........................\n\n",pid,leaderId);
	
	
}

void printGraphLeader(Node *node, char** s){
	int i;
	printf("PID=%d outward neighbours",pid);
	char name[10];
	sprintf(name, "dot_%d_%d.dot", fcount,pid);
	fcount++;
	FILE *fp=fopen(name,"w");
	fprintf(fp, "a%d [label=\"%d\"]\n",pid,pid);
	for(i=0;i<node->orgOutwardDelta;i++){
		
		printf("%d\t",node->fwdEdgeNbrs[i].id);	
		fprintf(fp, "a%d->a%d [label=\"%s\"]\n",pid,node->fwdEdgeNbrs[i].id,s[i]);
	
	}
	fclose(fp);
	printf("\n");
}


//Choose leader from inward sweep
int chooseSingledegreeLeader(Node* node,int newTag){

	//Send its Delta to all neighbours
//	printf("2####%x\n", node);
	char **s;//[node->orgOutwardDelta][60];
	s=(char**) malloc(node->orgOutwardDelta*sizeof(char*));
	
	
	int i;
	for(i=0;i<node->orgOutwardDelta;i++)
	{s[i]=(char*) malloc(60*sizeof(char));
	s[i][0]='\0';
	}
	int mssgRcvd[MAXNODES];
	for(i=0;i<MAXNODES;i++){
		mssgRcvd[i]=0;
	}
//	mssgRcvd[pid]=1;
	
	int numMssgRcvd = 0;

// 	int delta = node->inwardDelta+node->outwardDelta;

	int leaderId = pid;
	int sent=0, recvd=0;
// 	int myMessageNum[MAXNODES];
// 	myMessageNum[0] = pid;

	int myMessageNum;
	myMessageNum = pid;

	int sending=0;

	for(i=0;i<node->orgOutwardDelta;i++){
	
		MPI_Request request;

//		MPI_Isend(&myMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD,&request);
		MPI_Bsend(&myMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, newTag, MPI_COMM_WORLD);
		sent++;
		glbMsgSent++;
		if(DEBUG) printf("Sending Message from %d to %d, srcID=%d\n",pid,node->fwdEdgeNbrs[i].id,pid);
		sprintf(s[i], "%s %d",s[i],pid);
	}

	
	numMssgRcvd=0;
// 	MPI_Request request1;
	long counter = 1000000;
	
	while(numMssgRcvd < (node->sizeOfD)-1 && counter>0){
	
		int senderId;
// 		int rcvdMeesageNum[100];
		int rcvdMeesageNum;

		counter--;
		MPI_Status status; int flag;
		
		
		MPI_Iprobe(MPI_ANY_SOURCE, newTag, MPI_COMM_WORLD, &flag, &status);
		
		if(flag) 
		{
//			if(DEBUG) printf("PID=%d Waiting for message numMssgRcvd= %d\n",pid,numMssgRcvd);
/*			MPI_Recv(rcvdMeesageNum, 100,MPI_INT, MPI_ANY_SOURCE, newTag, MPI_COMM_WORLD, &status);*/
			MPI_Recv(&rcvdMeesageNum, 1,MPI_INT, MPI_ANY_SOURCE, newTag, MPI_COMM_WORLD, &status);
			glbMsgRcvd++;
			counter = 1000000;
			recvd++;
			senderId = status.MPI_SOURCE;
			
/*			int messageSrcId = rcvdMeesageNum[0];*/
			int messageSrcId = rcvdMeesageNum;

	//		int messageDelta = rcvdMeesageNum[0]/100;
	
			if(DEBUG) printf("Received Message from %d to %d, srcID=%d\n",senderId,pid,messageSrcId);
	
			if(messageSrcId==pid){
				printf("FATAL ERROR.............................:PID=%d gets its own message\n",pid);
			}
	
			if(mssgRcvd[messageSrcId]==0){
				
//				if(DEBUG) printf("Havent forwarded this message yet\n");
	
				if(leaderId>messageSrcId){
					leaderId=messageSrcId;
				}
				
				
//				if(DEBUG) printf("Going to forward the message \n");
	
				numMssgRcvd++;
				mssgRcvd[messageSrcId]=1;
	
				int forwarding=0;			
	
				for(i=0;i<node->orgOutwardDelta;i++){
					
					
	// 				if(node->fwdEdgeNbrs[i].id!=senderId && node->fwdEdgeNbrs[i].id!=messageSrcId && node->fwdEdgeNbrs[i].id!=pid && senderId!=pid && messageSrcId!=pid){
	
					if(node->fwdEdgeNbrs[i].id!=senderId && node->fwdEdgeNbrs[i].id!=messageSrcId && messageSrcId!=pid){
	
/*						int fwdMessageNum[100];
						fwdMessageNum[0] = rcvdMeesageNum[0];*/
						int fwdMessageNum;
						fwdMessageNum = rcvdMeesageNum;
			
						MPI_Bsend(&fwdMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, newTag, MPI_COMM_WORLD);
						glbMsgSent++;
						/*					MPI_Isend(&fwdMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD,&request1);*/
	
						if(DEBUG) printf("Sent Message from %d to %d, srcID=%d\n",pid,node->fwdEdgeNbrs[i].id,fwdMessageNum);
						sprintf(s[i], "%s,%d",s[i],fwdMessageNum);
	
					}else{
						if(DEBUG) printf("Message can not be sent to %d from %d...isRemoved=%d,senderId %d\n",node->fwdEdgeNbrs[i].id,pid,node->fwdEdgeNbrs[i].isRemoved,senderId);
					}
					
				}
			}
		}
	}	
	printf("PID=%d Leader choosed: leaderId=%d..........................\n\n",pid,leaderId);
	
	printGraphLeader(node,s);
	return leaderId;
	
	
}


//Partition the graph in SCC
int sCCPartition(Node* node, int sccLeader,int newTag){

	//Send its Delta to all neighbours
//	if(DEBUG) printf("2####%x\n", node);
	int i;
	int mssgRcvd[MAXNODES];
	for(i=0;i<MAXNODES;i++){
		mssgRcvd[i]=0;
	}
//	mssgRcvd[pid]=1;
	
	int numMssgRcvd = 0;

	int delta = node->inwardDelta+node->outwardDelta;

	int leaderId = pid;
	int sent=0, recvd=0;
	int myMessageNum[MAXNODES];
	
	for(i=0;i<MAXNODES;i++){
		myMessageNum[i] = -1;
	}

	myMessageNum[0] = 1;
	myMessageNum[1] = pid;

	int sending=0;

	
	if(sccLeader==pid){
		for(i=0;i<node->orgOutwardDelta;i++){
		
			MPI_Request request;
	
	//		MPI_Isend(&myMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD,&request);
			MPI_Bsend(myMessageNum,MAXNODES, MPI_INT, node->fwdEdgeNbrs[i].id,newTag, MPI_COMM_WORLD);
			sent++;
			glbMsgSent++;
			printf("Sending Message from %d to %d, srcID=%d\n",pid,node->fwdEdgeNbrs[i].id,pid);
			
		}
	}
	
	numMssgRcvd=0;
// 	MPI_Request request1;
	int mySCC[MAXNODES];
		
	int u;
	for(u=0;u<MAXNODES;u++){
		mySCC[u] = -1;	
	}
	
	int mySCCSize=0;
	
	long counter=10000000;
	
	while(counter>0){
		counter--;

		int senderId;
		int rcvdMeesageNum[MAXNODES];
		
		MPI_Status status; int flag;
		

		MPI_Iprobe(MPI_ANY_SOURCE, newTag, MPI_COMM_WORLD, &flag, &status);
		if(flag)
			{	
				MPI_Recv(rcvdMeesageNum, MAXNODES,MPI_INT, MPI_ANY_SOURCE, newTag, MPI_COMM_WORLD, &status);
				counter=10000000;
				recvd++;
				glbMsgRcvd++;
				senderId = status.MPI_SOURCE;
				int stampLength=rcvdMeesageNum[0];
				
				int messageSrcId = rcvdMeesageNum[1];
				
				int buffer[MAXNODES];
				
		
				if(DEBUG)printf("Received Message from %d to %d, srcID=%d with stamplength %d\n",senderId,pid,messageSrcId, stampLength);
		
/*				if(DEBUG) printf("stamplenght");
				int stml;
				for(stml=1;stml<=stampLength;stml++){
					printf("%d ",rcvdMeesageNum[stml]);
				}
				printf("\n");*/
				
				if(messageSrcId==pid){
							int i;
							for(i=0;i<stampLength;i++)
								{
									mySCC[rcvdMeesageNum[i+1]] =1;
									if(DEBUG) printf("PID=%d NODE added to my mySCC with id=%d\n ",pid,rcvdMeesageNum[i+1]);
								}
				}else if(messageSrcId==sccLeader){
				
				
	//				if(DEBUG) printf("Going to forward the message \n");
		
					//Decide whether to forward message or not
					
					int s;
					int myL = -1;
					int alreadyFWD = 0;
					
					rcvdMeesageNum[stampLength+1] = pid;
					rcvdMeesageNum[0] = rcvdMeesageNum[0]+1;
					
					for(i=0;i<node->orgOutwardDelta;i++){
						
						alreadyFWD = 0;
						for(s=2;s<MAXNODES;s++){
							if(rcvdMeesageNum[s]==pid && rcvdMeesageNum[s+1]==node->fwdEdgeNbrs[i].id){
								alreadyFWD = 1;
								break;
							}
						}

		
		// 				if(node->fwdEdgeNbrs[i].id!=senderId && node->fwdEdgeNbrs[i].id!=messageSrcId && node->fwdEdgeNbrs[i].id!=pid && senderId!=pid && messageSrcId!=pid){
		
						if(!alreadyFWD){
		
							
							int newMessageNum[MAXNODES];
							int te;
							for(te=0;te<MAXNODES;te++){
								newMessageNum[te] = rcvdMeesageNum[te];
							}
//							int fwdMessageNum[MAXNODES];
//							fwdMessageNum[0] = rcvdMeesageNum[0];
				
							MPI_Bsend(newMessageNum,MAXNODES, MPI_INT, node->fwdEdgeNbrs[i].id, newTag, MPI_COMM_WORLD);
							glbMsgSent++;
		/*					MPI_Isend(&fwdMessageNum,1, MPI_INT, node->fwdEdgeNbrs[i].id, 1, MPI_COMM_WORLD,&request1);*/
		
							if(DEBUG)	printf("Sent Message from %d to %d with message in sccpartition\n",pid,node->fwdEdgeNbrs[i].id);
							
							int j;
	//						for(j=0;j<newMessageNum[0];j++)
	//							if(DEBUG) printf(" %d  ",newMessageNum[j+1]);
	//						if(DEBUG) printf("\n");
							
		
						}else{
				//			printf("Message can not be sent to %d from %d...isRemoved=%d,senderId...already forwarded %d\n",node->fwdEdgeNbrs[i].id,pid,node->fwdEdgeNbrs[i].isRemoved,senderId);
						}
					}
				}
			}
	}
	
	printf("SCC of PID=%d is ",pid);
	for(i=0;i<MAXNODES;i++)
	{
		if(mySCC[i] !=-1){
		
			printf("%d\t",i);
		}
	}
	printf("\n");
	
	if(sccLeader==pid){
		node->isInSCC = 1;
		printf("I am in SCC\n");

		
		for(i=0;i<MAXNODES;i++){
		
			node->nodeSCC[i] = mySCC[i];
			if(mySCC[i]!=-1){
				node->sizeOfSCC = node->sizeOfSCC+1;
			}
		}
		//change
		mySCC[pid]=1;
		
		for(i=0;i<node->orgOutwardDelta;i++){
			printf("Leader %d sending SCC message to %d\n",sccLeader,node->fwdEdgeNbrs[i].id);
			MPI_Bsend(mySCC,MAXNODES, MPI_INT, node->fwdEdgeNbrs[i].id, newTag+1, MPI_COMM_WORLD);
			glbMsgSent++;
		}	
	}else{
		MPI_Status fstatus;
		int sourceId;
		while(1){	
			MPI_Recv(mySCC, MAXNODES,MPI_INT, MPI_ANY_SOURCE, newTag+1, MPI_COMM_WORLD, &fstatus);
			sourceId = fstatus.MPI_SOURCE;
			glbMsgRcvd++;
			printf("PID=%d, Leader %d received SCC message from %d\n",pid,sccLeader,sourceId);

			if(mySCC[sccLeader]!=-1){
				break;
			}
		}
		if(mySCC[pid]!=-1){
			node->isInSCC = 1;
			printf("I am in SCC\n");
			
			for(i=0;i<MAXNODES;i++){
		
				node->nodeSCC[i] = mySCC[i];
				if(mySCC[i]!=-1){
					node->sizeOfSCC = node->sizeOfSCC+1;
				}
			}

		}
		
		for(i=0;i<node->orgOutwardDelta;i++){
			if(node->fwdEdgeNbrs[i].id!=sourceId){
			
				MPI_Bsend(mySCC,MAXNODES, MPI_INT, node->fwdEdgeNbrs[i].id, newTag+1, MPI_COMM_WORLD);
				glbMsgSent++;
			}	
		}	
	}
	
	printf("myScc:\n");
	for(i=0;i<MAXNODES;i++){
		if(node->nodeSCC[i]==1) 
		printf("%d ",i);
		
	}
	printf("\n");
	//Flush all extra messages

// 		printf("PID=%d Leader choosed: leaderId=%d..........................\n\n",pid,leaderId);
}

//Find shadow of SCC

int findShadow(Node* node,int newTag){

	printf("find shadow called\n");
	int i;
	int mssgRcvd[MAXNODES];
	
	for(i=0;i<MAXNODES;i++){
		mssgRcvd[i]=0;
	}
	int numMssgRcvd = 0;

	int leaderId = pid;
	int sent=0, recvd=0;
	
	int myNeighbours[MAXNODES];
	
	int inwardNbrsUnion[MAXNODES];
	for(i=0;i<MAXNODES;i++){
		inwardNbrsUnion[i] = -1;
	}
	int outwardNbrsUnion[MAXNODES];
	for(i=0;i<MAXNODES;i++){
		outwardNbrsUnion[i] = -1;
	}

	
	for(i=0;i<MAXNODES;i++){
		myNeighbours[i] = -1;
	}
	
	myNeighbours[0]=pid;
	myNeighbours[1]=0;
	myNeighbours[MAXNODES/2] = 0;
	
	int nbrsId;
	if(DEBUG) printf("My outward shadow: orgOutwardDelta=%d\n",node->orgOutwardDelta);
	for(i=0;i<node->orgOutwardDelta;i++){
		
		nbrsId = node->fwdEdgeNbrs[i].id;
		
//		if(node->nodeSCC[nbrsId]==-1){
			outwardNbrsUnion[nbrsId] = 1;
			myNeighbours[1] = myNeighbours[1]+1;
			myNeighbours[1+myNeighbours[1]] = nbrsId;
			if(DEBUG) printf("nbrsId=%d and myNeighbours[1]=%d and myNeighbours[myNeighbours[1]]=%d\n",nbrsId,myNeighbours[1],myNeighbours[1+myNeighbours[1]]);
//		}		
	}
	if(DEBUG) printf("\n");
		
	if(DEBUG) printf("My inward shadow:");
	for(i=0;i<node->orgInwardDelta;i++){
		
		nbrsId = node->bwdEdgeNbrs[i].id;
//		if(node->nodeSCC[nbrsId]==-1){
			
			inwardNbrsUnion[nbrsId] = 1;
			myNeighbours[MAXNODES/2] = myNeighbours[MAXNODES/2]+1;
			myNeighbours[MAXNODES/2+myNeighbours[MAXNODES/2]] = nbrsId;
			if(DEBUG) printf("%d\t",myNeighbours[MAXNODES/2+myNeighbours[MAXNODES/2]]);
//		}		
	}
	if(DEBUG) printf("\n");
	
	
	int sending=0;

	for(i=0;i<node->orgOutwardDelta;i++){
	
		if(node->nodeSCC[node->fwdEdgeNbrs[i].id]!=-1){
			MPI_Request request;
	
			MPI_Bsend(myNeighbours,100, MPI_INT, node->fwdEdgeNbrs[i].id, newTag, MPI_COMM_WORLD);
			sent++;
			glbMsgSent++;
			if(DEBUG) printf("Sending Message from %d to %d, srcID=%d\n",pid,node->fwdEdgeNbrs[i].id,pid);
		}	
	}

	
	
	numMssgRcvd=0;
	long counter = 1000000;
	
	int su;
	
	
	printf("My sizeofSCC = %d\n",node->sizeOfSCC);
	
	while(numMssgRcvd < (node->sizeOfSCC)-1){
	
		int senderId;
		int rcvdMeesageNum[MAXNODES];

		counter--;
		MPI_Status status; int flag;
		
		
		MPI_Iprobe(MPI_ANY_SOURCE, newTag, MPI_COMM_WORLD, &flag, &status);
		
		if(flag) 
		{
			if(DEBUG) printf("PID=%d Waiting for message numMssgRcvd= %d\n",pid,numMssgRcvd);
			MPI_Recv(rcvdMeesageNum, 100,MPI_INT, MPI_ANY_SOURCE, newTag, MPI_COMM_WORLD, &status);
			counter = 1000000;
			recvd++;
			glbMsgRcvd++;
			senderId = status.MPI_SOURCE;
			
			int outwardSize = rcvdMeesageNum[1];
			int inwardSize = rcvdMeesageNum[MAXNODES/2];
			int messageSrcId = rcvdMeesageNum[0];
			
			
/*			int messageSrcId = rcvdMeesageNum[0];*/
	//		int messageDelta = rcvdMeesageNum[0]/100;
	
			if(DEBUG) printf("Received Message from %d to %d, srcID=%d\n",senderId,pid,messageSrcId);
				
			if(messageSrcId==pid && node->nodeSCC[messageSrcId]==-1){
				printf("FATAL ERROR............................either.:PID=%d gets its own message or node->nodeSCC[messageSrcId]=%d\n",pid,node->nodeSCC[messageSrcId]);
			}
	
			if(mssgRcvd[messageSrcId]==0){
				
				
				
				if(DEBUG) printf("Havent forwarded this message yet\n");
	
				if(DEBUG) printf("Including in outwoard union:");
				for(i=2;i<outwardSize+2;i++){
					outwardNbrsUnion[rcvdMeesageNum[i]]=1;
					node->outGraph[messageSrcId][i-2]=rcvdMeesageNum[i];
					if(DEBUG) printf("Adding %d\t",rcvdMeesageNum[i]);
				}
				if(DEBUG) printf("\n");
				
				if(DEBUG) printf("Including in inward union:");
				for(i=MAXNODES/2+1;i<MAXNODES/2+inwardSize+1;i++){
					inwardNbrsUnion[rcvdMeesageNum[i]] = 1;
					node->inGraph[messageSrcId][i-MAXNODES/2-1]=rcvdMeesageNum[i];
					if(DEBUG) printf("Adding %d\t",rcvdMeesageNum[i]);
				}
				if(DEBUG) printf("\n");
				
				

				
				
				if(DEBUG) printf("Going to forward the message \n");
	
				numMssgRcvd++;
				mssgRcvd[messageSrcId]=1;
	
				int forwarding=0;			
	
				for(i=0;i<node->orgOutwardDelta;i++){
					
					if(node->nodeSCC[node->fwdEdgeNbrs[i].id]!=-1 && node->fwdEdgeNbrs[i].id!=senderId && node->fwdEdgeNbrs[i].id!=messageSrcId){
	
		
						MPI_Bsend(rcvdMeesageNum,100, MPI_INT, node->fwdEdgeNbrs[i].id, newTag, MPI_COMM_WORLD);
						glbMsgSent++;
						if(DEBUG) printf("Sent Message from %d to %d, srcID=%d\n",pid,node->fwdEdgeNbrs[i].id,rcvdMeesageNum[0]);
	
					}else{
						if(DEBUG) printf("Message can not be sent to %d from %d...isRemoved=%d,senderId %d\n",node->fwdEdgeNbrs[i].id,pid,node->fwdEdgeNbrs[i].isRemoved,senderId);
					}
				}
			}
		}
	}	
	
	int inwardUnionSize=0;
	int outwardUnionSize=0;

	printf("Inward Union :\n");
	for(su=0;su<MAXNODES;su++){
		
		if(inwardNbrsUnion[su] == 1){
			printf("%d\t",su);
			inwardUnionSize++;
		}
		
	
	}
	printf("\n");
	printf("outward Union :");
	for(su=0;su<MAXNODES;su++){
		if(outwardNbrsUnion[su] == 1){
			printf("%d\t",su);
			outwardUnionSize++;
		}
		
	
	
	}
	printf("\n");

	printf("InwardUnionSize = %d and OutWardUnionSize=%d\n",inwardUnionSize,outwardUnionSize);
	
	// Inward Pruning Required => pruneIt=1
	// Outward Pruning Required => pruneIt=2
	// Both Pruning Required => pruneIt=3
/*	if((inwardUnionSize+node->sizeOfSCC)==sizeOfD)*/
/*	if((outwardUnionSize+node->sizeOfSCC)==sizeOfD)*/
	
/*	if((inwardUnionSize)==node->sizeOfD)
	{
		node->pruneIt=1;
	}
	if((outwardUnionSize)==node->sizeOfD)
	{
		node->pruneIt=node->pruneIt+2;
	}*/
	
	if((inwardUnionSize)==node->sizeOfD && (outwardUnionSize)==node->sizeOfD)
	{
		node->pruneIt=3;
	}else if((outwardUnionSize)==node->sizeOfD)
	{
		node->pruneIt=2;
	}else if((inwardUnionSize)==node->sizeOfD){
		node->pruneIt=1;
	}else{
		node->pruneIt=4;
		node->inwardUnionSize =inwardUnionSize;
		node->outwardUnionSize=outwardUnionSize;
	}
	
	
	if(node->pruneIt==3){
		printf("The complete graph is strongly connected. \n");
	}else if(node->pruneIt==2){
		printf("The graph is not strongly connected but SCC containing node %d has an outward shadow which covers the complete graph\n");
	}else if(node->pruneIt==1){
		printf("The graph is not strongly connected but SCC containing node %d has an inward shadow which covers the complete graph\n");
	
	}else if(node->pruneIt==4){
		printf("The graph is not strongly connected and this partition does not cover any shadow \n");
	
	}
	
	return 0;
	
	
}

int FindMinDeltaNode(int* deltaOfNodes){

	int i;
	int minDelta=-1;
	int minNodeId = -1;
	
//	minDelta=deltaOfNodes[0];
//	minNodeId =0;
	
	for(i=0;i<MAXNODES;i++){
		if(deltaOfNodes[i]!=0){
			if(minDelta==-1 || minDelta>deltaOfNodes[i]){
				minDelta = deltaOfNodes[i];
				minNodeId = i;
			}else if(minDelta==deltaOfNodes[i]){
			
				if(i<minNodeId){
					minNodeId=i;
				}
			}
		}	
	}
	return minNodeId;
	
}

int outBlack(Node* node, int *blk, int point,int *newOutwardShadow,int* DofSCC)
	{
		int i=0;
		int NumBlack=0;
		while(node->outGraph[point][i]!=-1)
		{
			newOutwardShadow[node->outGraph[point][i]] = 1;
//			printf("Adding node %d to newOutwardShadow and point=%d\n",node->outGraph[point][i],point);
			if(blk[node->outGraph[point][i]]==0 && DofSCC[node->outGraph[point][i]]==1)
			{
				blk[node->outGraph[point][i]]=1;
				NumBlack++;
				NumBlack+=outBlack(node,blk,node->outGraph[point][i],newOutwardShadow,DofSCC);
			}
			i++;
		}
/*		printf("Outwardshadow\n");
		for(i=0;i<MAXNODES;i++)
		{
			if(newOutwardShadow[i]!=-1)
			printf("%d ",i);
		}
		printf("\n");*/
		return NumBlack;
		
	}

int inBlack(Node* node,int *blk, int point,int *newInwardShadow,int* DofSCC)
	{
		int i=0;
		int NumBlack=0;
		while(node->inGraph[point][i]!=-1)
		{
			newInwardShadow[node->inGraph[point][i]] = 1;
//			printf("Adding node %d to newinwardShadow and point=%d\n",node->inGraph[point][i],point);

			if(blk[node->inGraph[point][i]]==0 && DofSCC[node->inGraph[point][i]]==1)
			{
				blk[node->inGraph[point][i]]=1;
				NumBlack++;
				NumBlack+=inBlack(node,blk,node->inGraph[point][i],newInwardShadow,DofSCC);
			}
			i++;
		}
		return NumBlack;
		
	}
	//Check connectivity and shadow at the time of pruning	
int connectivityCheck(Node* node,int leader, int *DofSCC, int sizeofDSCC,int pruneIt)
{
	int i=0;
	
	int newInwardShadow[MAXNODES];
	int newOutwardShadow[MAXNODES];
	
	for(i=0;i<MAXNODES;i++){
		newInwardShadow[i] = -1;
		newOutwardShadow[i] = -1;
	}
	
	int initial;
	i=0;
	while(node->outGraph[leader][i]!=-1)
	{	
		if(DofSCC[node->outGraph[leader][i]]==1)
		{	initial=node->outGraph[leader][i];
			break;
		}
		i++;
	}
/*	printf("Outwardshadow\n");
	for(i=0;i<MAXNODES;i++)
	{
		if(newOutwardShadow[i]!=-1)
			printf("%d ",i);
	}
	printf("\n");*/
	int blk[MAXNODES];
	for(i=0;i<MAXNODES;i++)
	{	
		blk[i]=0;
	}
	blk[leader]=1;
	int	NumBlack=1;
	
	blk[initial]=1;
	NumBlack++;
	
	i=0;
	while(node->outGraph[initial][i]!=-1)
	{
		newOutwardShadow[node->outGraph[initial][i]] = 1;
//		printf("Adding node %d to newOutwardShadow and Initial=%d\n",node->outGraph[initial][i],initial);

		if(blk[node->outGraph[initial][i]]==0 && DofSCC[node->outGraph[initial][i]]==1)
		{
			blk[node->outGraph[initial][i]]=1;
			NumBlack++;
			NumBlack+=outBlack(node,blk,node->outGraph[initial][i],newOutwardShadow,DofSCC);
		}
		i++;
	}
	
	if(NumBlack!=sizeofDSCC)	{printf("point 1 numblk=%d\n",NumBlack);return 0;}
	
	int outwardShadowSize=0;
//	printf("Printing outwardShadow:");
	for(i=0;i<MAXNODES;i++){
		if(newOutwardShadow[i]==1){
//			printf("%d\t",i);
			outwardShadowSize++;
		}
	}
	printf("\n");

	
	if(pruneIt==3||pruneIt==2){
		if(outwardShadowSize!=node->sizeOfD) {printf("point 2 outwardshdwsize=%d\n",outwardShadowSize);return 0;}
	}else if(pruneIt==4){
		if(outwardShadowSize!=node->outwardUnionSize) {printf("point 2 outwardshdwsize=%d\n",outwardShadowSize);return 0;}
	}
	// Inward Dirn
// 	i=0;
	while(node->inGraph[leader][i]!=-1)
	{	
		if(DofSCC[node->inGraph[leader][i]]==1)
		{	initial=node->inGraph[leader][i];
		break;
		}
		i++;
	}
	
	for(i=0;i<MAXNODES;i++)
	{	
		blk[i]=0;
	}
	blk[leader]=1;
	NumBlack=1;
	blk[initial]=1;
	NumBlack++;
	i=0;
	while(node->inGraph[initial][i]!=-1)
	{
		newInwardShadow[node->inGraph[initial][i]] = 1;
		
//		printf("Adding node %d to newinwardShadow and Initial=%d\n",node->inGraph[initial][i],initial);

		if(blk[node->inGraph[initial][i]]==0 && DofSCC[node->inGraph[initial][i]]==1)
		{
			blk[node->inGraph[initial][i]]=1;
			NumBlack++;
			NumBlack+=inBlack(node,blk,node->inGraph[initial][i],newInwardShadow,DofSCC);
		}
		i++;
	}
	
	if(NumBlack!=sizeofDSCC)	{printf("point 3 numblk=%d\n",NumBlack);return 0;}
	
	int inwardShadowSize=0;
//	printf("Printing inwardShadowSize\t");
	for(i=0;i<MAXNODES;i++){
		if(newInwardShadow[i]==1){
//			printf("%d\t",i);
			inwardShadowSize++;
		}
	}
	printf("\n");
	if(pruneIt==3||pruneIt==1){
		if(inwardShadowSize!=node->sizeOfD) {printf("point 4 inwardshdwsize=%d\n",inwardShadowSize);return 0;}
	}else if(pruneIt==4){
		if(inwardShadowSize!=node->inwardUnionSize) {printf("point 4 inwardshdwsize=%d\n",inwardShadowSize);return 0;}
	}
	
	return 1;
}

//mark neighbour black 
int markLeaderNbrs(Node* node,int leader,int* DofSCC,int* deltaOfNodes,int* FofSCC){

	int hasBlkOutward = 0;
	int hasBlkInward = 0;
	
	int whiteNodesRemoved=0;
	int i=0;
	
	int inwardNbrDelta=-1;
	int inwardNbrId = -1;
	
	i=0;
	while(node->inGraph[leader][i]!=-1){
		if(DofSCC[node->inGraph[leader][i]]==1){
			
			if(deltaOfNodes[node->inGraph[leader][i]]>0)
			deltaOfNodes[node->inGraph[leader][i]] = deltaOfNodes[node->inGraph[leader][i]] -1;
			if(FofSCC[node->inGraph[leader][i]]==1){
				hasBlkInward=1;
				break;
			}else{
				if(inwardNbrDelta==-1 || inwardNbrDelta>deltaOfNodes[node->inGraph[leader][i]]){
					inwardNbrDelta=deltaOfNodes[node->inGraph[leader][i]];
					inwardNbrId = node->inGraph[leader][i];
				}else if(inwardNbrDelta==deltaOfNodes[node->inGraph[leader][i]]){
					if(inwardNbrId>node->inGraph[leader][i]){
						inwardNbrId=node->inGraph[leader][i];
					}
				}
			}
		}
		i++;
	}
	
	if(hasBlkInward!=1){
		printf("Leader %d has no blk inward neighbour ..selecting %d for F",leader,inwardNbrId);
		FofSCC[inwardNbrId]=1;
		whiteNodesRemoved++;
		deltaOfNodes[inwardNbrId]=0;
	}

	
	int outwardNbrDelta=-1;
	int outwardNbrId = -1;
	
	i=0;
	while(node->outGraph[leader][i]!=-1){
		if(DofSCC[node->outGraph[leader][i]]==1){
			
			if(deltaOfNodes[node->outGraph[leader][i]]>0)
				deltaOfNodes[node->outGraph[leader][i]] = deltaOfNodes[node->outGraph[leader][i]] -1;
			if(FofSCC[node->outGraph[leader][i]]==1){
				hasBlkOutward=1;
				break;
			}else{
				if(outwardNbrDelta==-1 || outwardNbrDelta>deltaOfNodes[node->outGraph[leader][i]]){
					outwardNbrDelta=deltaOfNodes[node->outGraph[leader][i]];
					outwardNbrId = node->outGraph[leader][i];
				}else if(outwardNbrDelta==deltaOfNodes[node->outGraph[leader][i]]){
					if(outwardNbrId>node->outGraph[leader][i]){
						outwardNbrId=node->outGraph[leader][i];
					}
				}
			}
		}
		i++;
	}
	
	if(hasBlkOutward!=1){
		printf("Leader %d has no blk outward neighbour ..selecting %d for F",leader,outwardNbrId);
		FofSCC[outwardNbrId]=1;
		whiteNodesRemoved++;
		deltaOfNodes[outwardNbrId]=0;
	}
	return whiteNodesRemoved;
	
}

void printPrunedSet(Node *node){
	int i;
	
	
	char name[10];
	char color[10];
	strcpy(color, "darkolivegreen3");
	
	
	sprintf(name, "dot_%d_%d.dot", fcount,pid);
	fcount++;
	FILE *fp=fopen(name,"a");
 	
//	if (node->pruneIt==4) fprintf(fp, "a%d [label=\"%d\" color=\"bisque3\"]\n",pid,pid);
// 	else fprintf(fp, "a%d [label=\"%d\" color=\"seashell3\"]\n",pid,pid);
	for(i=0;i<node->orgOutwardDelta;i++){
		
		
		fprintf(fp, "%d->%d\n",pid,node->fwdEdgeNbrs[i].id);
	
	}
	fclose(fp);
	printf("\n");
}
int DofSCC[MAXNODES];

//algo for prunning
void pruneSet(Node *node)
{
	
	// inGraph[i][]
	// outGraph[i][]
	int i,j;
// 	for(i=0;i<MAXNODES;i++)
// 	{
// 		j=0;
// 		printf("\nIngraph of %d: ",i);
// 		while(node->inGraph[i][j]!=-1)
// 		{printf("%d ", node->inGraph[i][j]);
// 		j++;}
// 		j=0;
// 		printf("\nIngraph of %d: ",i);
// 		while(node->outGraph[i][j]!=-1)
// 		{printf("%d ", node->outGraph[i][j]);
// 		j++;}
// 	}
	
	
	int FofSCC[MAXNODES];
	
	int whiteNodesInD = node->sizeOfSCC;
	int sizeofDSCC = node->sizeOfSCC;
	
	
	for(i=0;i<MAXNODES;i++){
		DofSCC[i] = node->nodeSCC[i];
		FofSCC[i] = -1;
	}
	
	int deltaOfNodes[MAXNODES];
	
	for(i=0;i<MAXNODES;i++){
		int tempDelta=0;
		j=0;
		
		while(node->inGraph[i][j]!=-1){
			tempDelta++;
			j++;
		}
		j=0;
		while(node->outGraph[i][j]!=-1){
			tempDelta++;
			j++;
		}
		deltaOfNodes[i] = tempDelta;
	}
	
	
	int leader;
	while(whiteNodesInD>0){
		leader = FindMinDeltaNode(deltaOfNodes);
		printf("Selected %d for pruning\n", leader);
		int conResult = connectivityCheck(node,leader,DofSCC,sizeofDSCC,node->pruneIt);
		deltaOfNodes[leader] = 0;
		
		if(conResult==0){
			//Keep leader in graph
			FofSCC[leader] = 1;
			printf("%d cannot be pruned coz of disconnection\n", leader);
		}else{
			//Remove it from graph
			DofSCC[leader]=-1;
			sizeofDSCC--;
			//Mark nessary neighbours black and reduce all neighbours delta
			
			whiteNodesInD=whiteNodesInD-markLeaderNbrs(node,leader,DofSCC,deltaOfNodes,FofSCC);
		}
		
		
		
		whiteNodesInD--;
	}
	
	printf("Printing Nodes in F\n");
	for(i=0;i<MAXNODES;i++){
		if(FofSCC[i]==1){
		
			printf("%d\t",i);
		}
	}
	printf("\n");
// 	int min=10000;
	char name[10];
	sprintf(name, "dot_%d_%d.dot", fcount,pid);
	
// 	char color[]="darkolivegreen3";
// 	char color2[]="bisque3";
// 	
	char color[30];
	char color2[30];
	
	
	getColor1(leader,color);
	getColor2(leader,color2);
	FILE *fp=fopen(name,"w");
	printf("Printing Nodes in D\n");
// 	if(pid==leader){
// 		fprintf(fp, "subgraph cluster_1 { label =\"%s\" color=\"%s\"\n","SCC: Backbone",color);
// 		fprintf(fp, "label=\"%s\" color=\"%s\"\n","SCC: Backbone",color);
// 		fprintf(fp, "label=\"%s\" color=\"%s\"\n","SCC: Pruned Elements",color2);
// 
// 	}
//	if(pid==leader){
		for(i=0;i<MAXNODES;i++){
					if(DofSCC[i]==1){
	// 			if(min>i) min =i;
				fprintf(fp, "%d [label=\"%d\" color=\"%s\"]\n",i,i,color);
				printf("%d\t",i);
			}
			else if(node->nodeSCC[i]==1)
				fprintf(fp, "%d [label=\"%d\" color=\"%s\"]\n",i,i,color2);
				
		}
//	}
	printf("\n");
	
	fclose(fp);
// 	return  min;
	

}




void computeCDS(){

/*



*/
//	while(sizeOfD>0){
		//Intiate Leader Election Algorithm (Node with minimum degree will be choosed).
//		chooseSingleLeader();
//	}

}

void initializeNode(Node *node){

	node->isInSCC = -1;
	node->sizeOfSCC = 0;
	int i,j;
	for(i=0;i<MAXNODES;i++)
	{
		for(j=0;j<MAXNODES;j++)
		{
			node->inGraph[i][j]=-1;
			node->outGraph[i][j]=-1;

		}
	}
	node->pruneIt=0;
	
	node->inwardUnionSize =0;
	node->outwardUnionSize=0;

}

void initializeGraph(Node *node){

	int i;
	for(i=0;i<node->inwardDelta;i++){
		node->inGraph[pid][i] = node->bwdEdgeNbrs[i].id;
	}

	for(i=0;i<node->outwardDelta;i++){
		node->outGraph[pid][i] = node->fwdEdgeNbrs[i].id;
	}
}


void printGraph(Node *node){
	int i;
	printf("PID=%d outward neighbours",pid);
	char name[10];
	sprintf(name, "dot_%d_%d.dot", fcount,pid);
	fcount++;
	FILE *fp=fopen(name,"w");
	fprintf(fp, "a%d [label=\"%d\"]\n",pid,pid);
	for(i=0;i<node->orgOutwardDelta;i++){
		
		printf("%d\t",node->fwdEdgeNbrs[i].id);	
		fprintf(fp, "a%d->a%d\n",pid,node->fwdEdgeNbrs[i].id);
	
	}
	fclose(fp);
	printf("\n");
}

char filename[]="inward_Data";
char outfileName[]="outward_Data";

void readData(Node *node)
	{
		int i;
		
/*		char filename[]="inward_Data_9";*/
		//sprintf(filename,"%d_data",pid); // = "matrixData_8_1";
		FILE* fp=fopen(filename, "r");
		char line[75];
		
		fscanf(fp,"%d",&node->sizeOfD);
		printf("PID=%d, size of D=%d\n",pid,node->sizeOfD);
		
		
		for(i=0;i<=(pid);i++){
			fscanf(fp,"%s",line);
//			printf("PID=%d Skipping line\n",pid);
		}

		char* newword = (char*)strtok(line,",");
		
		node->inwardDelta=atoi(newword);
		node->orgInwardDelta = node->inwardDelta;
		
		printf("Node %d's inward neighbours are: ",pid);

		for(i=0;i<node->inwardDelta;i++)
		{	
			int nbr;
			newword=strtok(NULL , ",");
			nbr=atoi(newword);
			node->bwdEdgeNbrs[i].id=nbr;	//also set isRemoved and isBlack
			node->bwdEdgeNbrs[i].isRemoved=0;
			node->bwdEdgeNbrs[i].isBlack=0;

			printf("%d\t",nbr);
		}
		printf("\n");
		fclose(fp);

/*		char fileName[]="outward_Data_9";*/
		//sprintf(filename,"%d_data",pid); // = "matrixData_8_1";
		fp=fopen(outfileName, "r");
//		char line[75];
		
		for(i=0;i<=(pid);i++)
		{fscanf(fp,"%s",line);}
		char* integer;
		integer=(char*)strtok(line,",");
		node->outwardDelta=atoi(integer);
		node->orgOutwardDelta = node->outwardDelta;
		printf("Node %d's outward neighbours are: ",pid);
		for(i=0;i<node->outwardDelta;i++)
		{	
			int nbr;
			integer=strtok(NULL,",");
			nbr=atoi(integer);
			node->fwdEdgeNbrs[i].id=nbr;	//also set isRemoved and isBlack
			node->fwdEdgeNbrs[i].isRemoved=0;
			node->fwdEdgeNbrs[i].isBlack=0;

			printf("%d\t",nbr);
		}
		printf("\n");
		fclose(fp);
		
		
	}
		

void Solver(){

	
}

void getColor1(int leader,char* color){
	
	switch(leader)
	{
		case 0:		{strcpy(color, "darkslateblue");break;}
		case 1:		{strcpy(color, "lightgoldenrod4");break;}
		case 2:		{strcpy(color, "lightsalmon3");break;}
		case 3:		{strcpy(color, "powderblue");break;}
		case 4:		{strcpy(color, "khaki4");break;}
		case 5:		{strcpy(color, "darkolivegreen3");break;}
		case 6:		{strcpy(color, "navajowhite4");break;}
		case 7:		{strcpy(color, "paleturquoise4");break;}
		case 8:		{strcpy(color, "peru");break;}
		case 9:		{strcpy(color, "red4");break;}
		case 10:	{strcpy(color, "aquamarine4");break;}
		case 11:	{strcpy(color, "chartreuse4");break;}
		case 12:	{strcpy(color, "darkslategray");break;}
		case 13:	{strcpy(color, "limegreen");break;}
		case 14:	{strcpy(color, "orange1");break;}
		default:    {strcpy(color, "tomato");break;}
		
	}
}
void getColor2(int leader,char* color){
	
	switch(leader)
	{
		case 0:		{strcpy(color, "seashell3");break;}
		case 1:		{strcpy(color, "burlywood");break;}
		case 2:		{strcpy(color, "darkkhaki");break;}
		case 3:		{strcpy(color, "darkorchid3");break;}
		case 4:		{strcpy(color, "gold1");break;}
		case 5:		{strcpy(color, "yellow");break;}
		case 6:		{strcpy(color, "antiquewhite");break;}
		case 7:		{strcpy(color, "gray15");break;}
		case 8:		{strcpy(color, "lightblue3");break;}
		case 9:		{strcpy(color, "mistyrose");break;}
		case 10:		{strcpy(color, "thistle2");break;}
		case 11:		{strcpy(color, "skyblue");break;}
		case 12:		{strcpy(color, "plum3");break;}
		case 13:		{strcpy(color, "navajowhite");break;}
		case 14:		{strcpy(color, "blanchedalmond");break;}
		default:    {strcpy(color, "paleturquoise3");break;}
		
	}
}
int cnt=1;
void printSCC(Node *node, int leader){
	int i;
	cnt++;
	
	char name[10];
	char color[10];
	switch(leader)
	{
		case 0:		{strcpy(color, "darkslateblue");break;}
		case 1:		{strcpy(color, "lightgoldenrod2");break;}
		case 2:		{strcpy(color, "lightsalmon2");break;}
		case 3:		{strcpy(color, "powderblue");break;}
		case 4:		{strcpy(color, "khaki4");break;}
		case 5:		{strcpy(color, "darkolivegreen3");break;}
		case 6:		{strcpy(color, "navajowhite4");break;}
		case 7:		{strcpy(color, "paleturquoise4");break;}
		case 8:		{strcpy(color, "peru");break;}
		case 9:		{strcpy(color, "red4");break;}
		default:    {strcpy(color, "pink");break;}
		
	}
	
	sprintf(name, "dot_%d_%d.dot", fcount,pid);
	fcount++;
	FILE *fp=fopen(name,"w");
	fprintf(fp, "a%d [label=\"%d\" color=\"%s\"]\n",pid,pid,color);
	for(i=0;i<node->orgOutwardDelta;i++){
		
		
		fprintf(fp, "a%d->a%d\n",pid,node->fwdEdgeNbrs[i].id);
	
	}
	fclose(fp);
	printf("\n");
}


//Main function
int main(int argc,char* argv[]){

	
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &P); 
	printf("%d",P);
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);
	MPI_Buffer_attach( malloc(BUFFSIZE), BUFFSIZE); 
	sprintf(MsgFileName,"msgFile_%d",pid);
	msgFP = fopen(MsgFileName,"w");

	// 	MPI_Type_contiguous(sizeof(Message)/sizeof(int), MPI_INT, &msgType);
// 	MPI_Type_commit(&msgType);

//    printf("sizeof Message %d\n",sizeof(Message));

//	MPI_Buffer_attach( malloc(BUFFSIZE), BUFFSIZE); 

	Node node;
	initializeNode(&node);
	readData(&node);
	initializeGraph(&node);
	printf("####%x\n", &node);
//	MPI_Barrier(MPI_COMM_WORLD);
	
	printGraph(&node);
	int j=1;
	
	int sccLeader;
	while(node.isInSCC!=1){
	
		sccLeader =	chooseSingledegreeLeader(&node,j);
		
		fprintf(msgFP,"msgsent in chooseSingleLeader %d\n",glbMsgSent);
		fprintf(msgFP,"msgReceived in chooseSingleLeader %d\n",glbMsgRcvd);
	
		glbMsgSent =0;
		glbMsgRcvd = 0;

//		MPI_Barrier(MPI_COMM_WORLD);
		sCCPartition(&node,sccLeader,j+1);
		
		fprintf(msgFP,"msg sent in sccpartition %d\n",glbMsgSent);
		fprintf(msgFP,"msgReceived in sccpartition %d\n",glbMsgRcvd);
	
		glbMsgSent =0;
		glbMsgRcvd = 0;

		
		j=j+3;
	}
	printSCC(&node,sccLeader);	
	MPI_Barrier(MPI_COMM_WORLD);
	findShadow(&node,10);
	fprintf(msgFP,"msgsent in findshadow %d\n",glbMsgSent);
	fprintf(msgFP,"msgReceived in findshadow %d\n",glbMsgRcvd);
	
	glbMsgSent =0;
	glbMsgRcvd = 0;

	
	j++;
	fclose(msgFP);
	MPI_Barrier(MPI_COMM_WORLD);
	
	if(node.pruneIt!=0){
		pruneSet(&node);
		printf("Pruning Completed %d", pid);
	}
	printPrunedSet(&node);
	//------------------------------------------
	
	//------
	MPI_Finalize(); 
	//------
	
	return 0;
}
